﻿#include "pch.h"
#include "Sample3DSceneRenderer.h"

#include "..\Common\DirectXHelper.h"
#include "./Common.h"
#include <ppltasks.h>
#include <synchapi.h>

using namespace DirectX12;

using namespace Concurrency;
using namespace DirectX;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::Storage;

// Indizes in die Anwendungsstatuszuweisung.
Platform::String^ AngleKey = "Angle";
Platform::String^ TrackingKey = "Tracking";

// Lädt den Scheitelpunkt und die Pixel-Shader aus den Dateien und instanziiert die Würfelgeometrie.
Sample3DSceneRenderer::Sample3DSceneRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources) :
	m_loadingComplete(false),
	m_radiansPerSecond(XM_PIDIV4),	// 45 Grad pro Sekunde drehen
	m_angle(0),
	m_tracking(false),
	m_mappedConstantBuffer(nullptr),
	m_deviceResources(deviceResources)
{
	LoadState();
	ZeroMemory(&m_constantBufferData, sizeof(m_constantBufferData));

	CreateDeviceDependentResources();
	CreateWindowSizeDependentResources();
}

Sample3DSceneRenderer::~Sample3DSceneRenderer()
{
	m_constantBuffer->Unmap(0, nullptr);
	m_mappedConstantBuffer = nullptr;
}

void Sample3DSceneRenderer::CreateDeviceDependentResources()
{
	auto d3dDevice = m_deviceResources->GetD3DDevice();

	// Create root signature with constant buffers, shader resources and a sampler
	CreateRootSignature(d3dDevice);

	// Load shaders async
	auto createVSTask = DX::ReadDataAsync(L"SampleVertexShader.cso").then([this](std::vector<byte>& fileData)
	{
		m_vertexShader = fileData;
	});

	auto createPSTask = DX::ReadDataAsync(L"SamplePixelShader.cso").then([this](std::vector<byte>& fileData)
	{
		m_pixelShader = fileData;
	});

	// create pipeline state object.
	auto createPipelineStateTask = (createPSTask && createVSTask).then([this]()
	{
		CreatePSO();
	});

	// Würfelgeometrieressourcen erstellen und in die GPU hochladen.
	auto createAssetsTask = createPipelineStateTask.then([this]()
	{
		DX::ReadDataAsync(L"Assets\\grass.bmp").then([this](std::vector<byte>& filedata)
		{
			auto d3dDevice = m_deviceResources->GetD3DDevice();

			// Erstellt eine Befehlsliste.
			DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_pipelineState.Get(), IID_PPV_ARGS(&m_commandList)));
			NAME_D3D12_OBJECT(m_commandList);
			CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
			CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);

			XMFLOAT3 pos0 = XMFLOAT3(-0.5f, -0.5f, -0.5f);
			XMFLOAT3 pos1 = XMFLOAT3(-0.5f, -0.5f, 0.5f);
			XMFLOAT3 pos2 = XMFLOAT3(0.5f, -0.5f, -0.5f);
			XMFLOAT3 pos3 = XMFLOAT3(0.5f, -0.5f, 0.5f);

			XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

			Cross(pos2, pos0, &normal);

			BasicVertexStruct quadVertices[] =
			{
				{ pos0, XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), normal },
				{ pos1, XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), normal },
				{ pos2, XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), normal },
				{ pos3, XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f), normal },
			};

			const UINT vertexBufferSize = sizeof(quadVertices);

			// Vertexpufferressource im Standardheap der GPU erstellen und Indexdaten mit dem Uploadheap dort hinein kopieren.
			// Die Uploadressource darf erst freigegeben werden, wenn sie von der GPU nicht mehr verwendet wird.
			Microsoft::WRL::ComPtr<ID3D12Resource> vertexBufferUpload;

			CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);
			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&defaultHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&vertexBufferDesc,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&m_vertexBuffer)));
			NAME_D3D12_OBJECT(m_vertexBuffer);

			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&uploadHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&vertexBufferDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&vertexBufferUpload)));
			NAME_D3D12_OBJECT(vertexBufferUpload);

			// Vertexpuffer in die GPU hochladen.
			D3D12_SUBRESOURCE_DATA vertexData = {};
			vertexData.pData = reinterpret_cast<BYTE*>(quadVertices);
			vertexData.RowPitch = vertexBufferSize;
			vertexData.SlicePitch = vertexData.RowPitch;

			UpdateSubresources(m_commandList.Get(), m_vertexBuffer.Get(), vertexBufferUpload.Get(), 0, 0, 1, &vertexData);

			CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
			m_commandList->ResourceBarrier(1, &vertexBufferResourceBarrier);

			unsigned short quadIndices[] =
			{
				0, 2, 3,
				0, 3, 1,
			};

			const UINT indexBufferSize = sizeof(quadIndices);

			// Indexpufferressource im Standardheap der GPU erstellen und Indexdaten mit dem Uploadheap dort hinein kopieren.
			// Die Uploadressource darf erst freigegeben werden, wenn sie von der GPU nicht mehr verwendet wird.
			Microsoft::WRL::ComPtr<ID3D12Resource> indexBufferUpload;

			CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(indexBufferSize);
			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&defaultHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&indexBufferDesc,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&m_indexBuffer)));
			NAME_D3D12_OBJECT(m_indexBuffer);

			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&uploadHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&indexBufferDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&indexBufferUpload)));
			NAME_D3D12_OBJECT(indexBufferUpload);

			// Indexpuffer in die GPU hochladen.
			D3D12_SUBRESOURCE_DATA indexData = {};
			indexData.pData = reinterpret_cast<BYTE*>(quadIndices);
			indexData.RowPitch = indexBufferSize;
			indexData.SlicePitch = indexData.RowPitch;

			UpdateSubresources(m_commandList.Get(), m_indexBuffer.Get(), indexBufferUpload.Get(), 0, 0, 1, &indexData);

			CD3DX12_RESOURCE_BARRIER indexBufferResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_indexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
			m_commandList->ResourceBarrier(1, &indexBufferResourceBarrier);

			// Vertex-/Indexpufferansichten erstellen.
			m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
			m_vertexBufferView.StrideInBytes = sizeof(BasicVertexStruct);
			m_vertexBufferView.SizeInBytes = sizeof(quadVertices);

			m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress();
			m_indexBufferView.SizeInBytes = sizeof(quadIndices);
			m_indexBufferView.Format = DXGI_FORMAT_R16_UINT;

			// Erstellt einen Deskriptorheap für die Konstantenpuffer.
			D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
			heapDesc.NumDescriptors = DX::c_frameCount + 1;
			heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
			heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
			DX::ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&m_cbvSrvHeap)));
			NAME_D3D12_OBJECT(m_cbvSrvHeap);
			m_cbvSrvDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

			//Create cbv and srv buffers
			CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(DX::c_frameCount * c_alignedConstantBufferSize);
			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&uploadHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&constantBufferDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&m_constantBuffer)));
			NAME_D3D12_OBJECT(m_constantBuffer);

			// Erstellt Konstantenpufferansichten für den Zugriff auf den Uploadpuffer.
			D3D12_GPU_VIRTUAL_ADDRESS cbvSrvGpuAddress = m_constantBuffer->GetGPUVirtualAddress();
			CD3DX12_CPU_DESCRIPTOR_HANDLE cbvSrvCpuHandle(m_cbvSrvHeap->GetCPUDescriptorHandleForHeapStart());

			for (int n = 0; n < DX::c_frameCount; n++)
			{
				D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
				ZeroMemory(&desc, sizeof(D3D12_CONSTANT_BUFFER_VIEW_DESC));
				desc.BufferLocation = cbvSrvGpuAddress;
				desc.SizeInBytes = c_alignedConstantBufferSize;
				d3dDevice->CreateConstantBufferView(&desc, cbvSrvCpuHandle);

				cbvSrvGpuAddress += c_alignedConstantBufferSize;
				cbvSrvCpuHandle.Offset(m_cbvSrvDescriptorSize);
			}

			// Ordnet die Konstantenpuffer zu.
			CD3DX12_RANGE readRange(0, 0);		// Wir beabsichtigen nicht, aus dieser Ressource für die CPU zu lesen.
			DX::ThrowIfFailed(m_constantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedConstantBuffer)));
			ZeroMemory(m_mappedConstantBuffer, DX::c_frameCount * c_alignedConstantBufferSize);
			// Wir heben die Zuordnung erst auf, wenn die App geschlossen wird. Es ist ohne Weiteres möglich, die Zuordnung bis zum Ende der Ressourcenlebensdauer beizubehalten.

			D3D12_RESOURCE_DESC textureDesc = {};
			textureDesc.MipLevels = 1;
			textureDesc.Format = DXGI_FORMAT_B5G6R5_UNORM;
			textureDesc.Width = 512;
			textureDesc.Height = 512;
			textureDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
			textureDesc.DepthOrArraySize = 1;
			textureDesc.SampleDesc.Count = 1;
			textureDesc.SampleDesc.Quality = 0;
			textureDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

			// create texture
			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&defaultHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&textureDesc,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&m_textureBuffer)));
			NAME_D3D12_OBJECT(m_textureBuffer);

			// Describe and create a SRV for the texture.
			D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
			ZeroMemory(&srvDesc, sizeof(D3D12_SHADER_RESOURCE_VIEW_DESC));
			srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
			srvDesc.Format = textureDesc.Format;
			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
			srvDesc.Texture2D.MipLevels = 1;
			d3dDevice->CreateShaderResourceView(m_textureBuffer.Get(), &srvDesc, cbvSrvCpuHandle);
			cbvSrvCpuHandle.Offset(m_cbvSrvDescriptorSize);

			const UINT64 textureBufferSize = GetRequiredIntermediateSize(m_textureBuffer.Get(), 0, 1);
			// Copy data to the intermediate upload heap and then schedule a copy 
			// from the upload heap to the Texture2D.
			ComPtr<ID3D12Resource> textureUpload;
			// Create the GPU upload buffer.
			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&uploadHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(textureBufferSize),
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&textureUpload)));
			NAME_D3D12_OBJECT(textureUpload);

			D3D12_SUBRESOURCE_DATA textureData = {};
			textureData.pData = &filedata[0];
			textureData.RowPitch = 512 * 2;
			textureData.SlicePitch = textureData.RowPitch * 512;

			UpdateSubresources(m_commandList.Get(), m_textureBuffer.Get(), textureUpload.Get(), 0, 0, 1, &textureData);
			m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_textureBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

			// Create sampler desc heap
			D3D12_DESCRIPTOR_HEAP_DESC samplerHeapDesc = {};
			samplerHeapDesc.NumDescriptors = 1;
			samplerHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
			samplerHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
			DX::ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&samplerHeapDesc, IID_PPV_ARGS(&m_samplerHeap)));
			NAME_D3D12_OBJECT(m_samplerHeap);
			m_samplerDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER);

			D3D12_SAMPLER_DESC sampler = {};
			ZeroMemory(&sampler, sizeof(D3D12_SAMPLER_DESC));
			sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
			sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			sampler.MinLOD = 0;
			sampler.MaxLOD = D3D12_FLOAT32_MAX;
			sampler.MipLODBias = 0.0f;
			sampler.MaxAnisotropy = 1;
			sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
			d3dDevice->CreateSampler(&sampler, m_samplerHeap->GetCPUDescriptorHandleForHeapStart());

			// Befehlsliste schließen und ausführen, um mit dem Kopieren des Vertex-/Indexpuffers in den Standardheap der GPU zu beginnen.
			DX::ThrowIfFailed(m_commandList->Close());
			ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
			m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

			// Auf das Beenden der Ausführung der Befehlsliste warten. Die Vertex-/Indexpuffer müssen in die GPU hochgeladen werden, bevor die Uploadressourcen den Bereich überschreiten.
			m_deviceResources->WaitForGpu();

			m_loadingComplete = true;
		});
	});
}

void DirectX12::Sample3DSceneRenderer::CreatePSO()
{
	static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0,DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 }
	};

	D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
	state.InputLayout = { inputLayout, _countof(inputLayout) };
	state.pRootSignature = m_rootSignature.Get();
	state.VS = CD3DX12_SHADER_BYTECODE(&m_vertexShader[0], m_vertexShader.size());
	state.PS = CD3DX12_SHADER_BYTECODE(&m_pixelShader[0], m_pixelShader.size());
	state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	state.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	state.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	state.SampleMask = UINT_MAX;
	state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	state.NumRenderTargets = 1;
	state.RTVFormats[0] = m_deviceResources->GetBackBufferFormat();
	state.DSVFormat = m_deviceResources->GetDepthBufferFormat();
	state.SampleDesc.Count = 1;

	DX::ThrowIfFailed(m_deviceResources->GetD3DDevice()->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&m_pipelineState)));

	// PSO has been created. We dont need the shaders anymore
	m_vertexShader.clear();
	m_pixelShader.clear();
}

void DirectX12::Sample3DSceneRenderer::CreateRootSignature(ID3D12Device * d3dDevice)
{
	CD3DX12_DESCRIPTOR_RANGE descRange[3];
	CD3DX12_ROOT_PARAMETER rootParams[3];

	descRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	descRange[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
	descRange[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);

	rootParams[0].InitAsDescriptorTable(1, &descRange[0], D3D12_SHADER_VISIBILITY_ALL);
	rootParams[1].InitAsDescriptorTable(1, &descRange[1], D3D12_SHADER_VISIBILITY_PIXEL);
	rootParams[2].InitAsDescriptorTable(1, &descRange[2], D3D12_SHADER_VISIBILITY_PIXEL);

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Nur der Eingabeassemblerzustand benötigt Zugriff auf den Konstantenpuffer.
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;

	CD3DX12_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init(_countof(rootParams), rootParams, 0, nullptr, rootSignatureFlags);

	ComPtr<ID3DBlob> pSignature;
	ComPtr<ID3DBlob> pError;
	DX::ThrowIfFailed(D3D12SerializeRootSignature(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_rootSignature)));
	NAME_D3D12_OBJECT(m_rootSignature);
}

// Initialisiert Anzeigeparameter, wenn sich die Fenstergröße ändert.
void Sample3DSceneRenderer::CreateWindowSizeDependentResources()
{
	Size outputSize = m_deviceResources->GetOutputSize();
	float aspectRatio = outputSize.Width / outputSize.Height;
	float fovAngleY = 70.0f * XM_PI / 180.0f;

	D3D12_VIEWPORT viewport = m_deviceResources->GetScreenViewport();
	m_scissorRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) };

	// Dies ist ein einfaches Beispiel für eine Änderung, die vorgenommen werden kann, wenn die App im
	// Hochformat oder angedockte Ansicht.
	if (aspectRatio < 1.0f)
	{
		fovAngleY *= 2.0f;
	}

	// Die OrientationTransform3D-Matrix wird hier im Nachhinein multipliziert
	// um die Szene in Bezug auf die Bildschirmausrichtung ordnungsgemäß auszurichten.
	// Dieser anschließende Multiplikationsschritt ist für alle Zeichnen-Befehle erforderlich, die
	// auf das Renderziel der Swapchain angewendet werden. Diese Transformation sollte nicht auf Zeichnen-Befehle
	// für andere Ziele angewendet werden.

	// Für dieses Beispiel wird ein rechtshändiges Koordinatensystem mit Zeilenmatrizen verwendet.
	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovRH(
		fovAngleY,
		aspectRatio,
		0.01f,
		100.0f
	);

	XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	XMMATRIX orientationMatrix = XMLoadFloat4x4(&orientation);

	XMStoreFloat4x4(
		&m_constantBufferData.projectionMatrix,
		XMMatrixTranspose(perspectiveMatrix * orientationMatrix)
	);

	// Das Auge befindet sich bei (0,0.7,1.5) und betrachtet Punkt (0,-0.1,0) mit dem Up-Vektor entlang der Y-Achse.
	static const XMVECTORF32 eye = { 0.0f, 0.7f, 1.5f, 0.0f };
	static const XMVECTORF32 at = { 0.0f, -0.1f, 0.0f, 0.0f };
	static const XMVECTORF32 up = { 0.0f, 1.0f, 0.0f, 0.0f };

	static const DirectX::XMVECTORF32 lightDirection = { 0.0f, 1.0f, 0.0f };
	static const DirectX::XMVECTORF32 cameraPosition = { eye[0], eye[1], eye[2] };

	XMStoreFloat4x4(&m_constantBufferData.viewMatrix, XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up)));
	XMStoreFloat3(&m_constantBufferData.lightDirection, lightDirection);
	XMStoreFloat3(&m_constantBufferData.cameraPosition, cameraPosition);
}

// Wird einmal pro Frame aufgerufen, dreht den Würfel und berechnet das Modell und die Anzeigematrizen.
void Sample3DSceneRenderer::Update(DX::StepTimer const& timer)
{
	if (m_loadingComplete)
	{
		if (!m_tracking)
		{
			// Den Würfel nur gering drehen.
			m_angle += static_cast<float>(timer.GetElapsedSeconds()) * m_radiansPerSecond;

			Rotate(m_angle);
		}

		// Die Konstantenpufferressource aktualisieren.
		UINT8* destination = m_mappedConstantBuffer + (m_deviceResources->GetCurrentFrameIndex() * c_alignedConstantBufferSize);
		memcpy(destination, &m_constantBufferData, sizeof(m_constantBufferData));
	}
}

// Speichert den aktuellen Zustand des Renderers.
void Sample3DSceneRenderer::SaveState()
{
	auto state = ApplicationData::Current->LocalSettings->Values;

	if (state->HasKey(AngleKey))
	{
		state->Remove(AngleKey);
	}
	if (state->HasKey(TrackingKey))
	{
		state->Remove(TrackingKey);
	}

	state->Insert(AngleKey, PropertyValue::CreateSingle(m_angle));
	state->Insert(TrackingKey, PropertyValue::CreateBoolean(m_tracking));
}

// Stellt den vorherigen Zustand des Renderers wieder her.
void Sample3DSceneRenderer::LoadState()
{
	auto state = ApplicationData::Current->LocalSettings->Values;
	if (state->HasKey(AngleKey))
	{
		m_angle = safe_cast<IPropertyValue^>(state->Lookup(AngleKey))->GetSingle();
		state->Remove(AngleKey);
	}
	if (state->HasKey(TrackingKey))
	{
		m_tracking = safe_cast<IPropertyValue^>(state->Lookup(TrackingKey))->GetBoolean();
		state->Remove(TrackingKey);
	}
}

// Das 3D-Würfelmodell um ein festgelegtes Bogenmaß drehen.
void Sample3DSceneRenderer::Rotate(float radians)
{
	// Auf das Übergeben der aktualisierten Modellmatrix an den Shader vorbereiten.
	XMStoreFloat4x4(&m_constantBufferData.modelMatrix, XMMatrixTranspose(XMMatrixRotationY(radians)));
}

void Sample3DSceneRenderer::StartTracking()
{
	m_tracking = true;
}

// Bei der Nachverfolgung kann der 3D-Würfel um seine Y-Achse gedreht werden, indem die Zeigerposition relativ zur Breite des Ausgabebildschirms nachverfolgt wird.
void Sample3DSceneRenderer::TrackingUpdate(float positionX)
{
	if (m_tracking)
	{
		float radians = XM_2PI * 2.0f * positionX / m_deviceResources->GetOutputSize().Width;
		Rotate(radians);
	}
}

void Sample3DSceneRenderer::StopTracking()
{
	m_tracking = false;
}

// Rendert einen Frame mit dem Scheitelpunkt und den Pixel-Shadern.
bool Sample3DSceneRenderer::Render()
{
	// Der Ladevorgang verläuft asynchron. Geometrien erst nach dem Laden zeichnen.
	if (!m_loadingComplete)
	{
		return false;
	}

	DX::ThrowIfFailed(m_deviceResources->GetCommandAllocator()->Reset());

	// Die Befehlsliste kann jederzeit zurückgesetzt werden, nachdem "ExecuteCommandList()" aufgerufen wurde.
	DX::ThrowIfFailed(m_commandList->Reset(m_deviceResources->GetCommandAllocator(), m_pipelineState.Get()));

	PIXBeginEvent(m_commandList.Get(), 0, L"Draw the cube");
	{
		// Die von diesem Frame verwendete Grafikstammsignatur und die Deskriptorheaps festlegen.
		m_commandList->SetGraphicsRootSignature(m_rootSignature.Get());
		ID3D12DescriptorHeap* ppHeaps[] = { m_cbvSrvHeap.Get(), m_samplerHeap.Get() };
		m_commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

		// Bindet den Konstantenpuffer des aktuellen Frames an die Pipeline.
		auto currentFrameIndex = m_deviceResources->GetCurrentFrameIndex();
		CD3DX12_GPU_DESCRIPTOR_HANDLE cbvGpuHandle(m_cbvSrvHeap->GetGPUDescriptorHandleForHeapStart(), currentFrameIndex, m_cbvSrvDescriptorSize);
		m_commandList->SetGraphicsRootDescriptorTable(0, cbvGpuHandle);
		CD3DX12_GPU_DESCRIPTOR_HANDLE srvGpuHandle(m_cbvSrvHeap->GetGPUDescriptorHandleForHeapStart(), 3, m_cbvSrvDescriptorSize);
		m_commandList->SetGraphicsRootDescriptorTable(1, srvGpuHandle);
		m_commandList->SetGraphicsRootDescriptorTable(2, m_samplerHeap->GetGPUDescriptorHandleForHeapStart());

		// Viewport- und Scherenrechteck festlegen.
		D3D12_VIEWPORT viewport = m_deviceResources->GetScreenViewport();
		m_commandList->RSSetViewports(1, &viewport);
		m_commandList->RSSetScissorRects(1, &m_scissorRect);

		// Angeben, dass diese Ressource als Renderziel verwendet wird.
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_commandList->ResourceBarrier(1, &renderTargetResourceBarrier);

		// Zeichenbefehle aufzeichnen.
		D3D12_CPU_DESCRIPTOR_HANDLE renderTargetView = m_deviceResources->GetRenderTargetView();
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
		m_commandList->ClearRenderTargetView(renderTargetView, DirectX::Colors::CornflowerBlue, 0, nullptr);
		m_commandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

		m_commandList->OMSetRenderTargets(1, &renderTargetView, false, &depthStencilView);

		m_commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		m_commandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);
		m_commandList->IASetIndexBuffer(&m_indexBufferView);
		m_commandList->DrawIndexedInstanced(6, 1, 0, 0, 0);

		// Angeben, dass das Renderziel nicht zum Präsentieren verwendet wird, wenn die Ausführung der Befehlsliste beendet ist.
		CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
		m_commandList->ResourceBarrier(1, &presentResourceBarrier);
	}
	PIXEndEvent(m_commandList.Get());

	DX::ThrowIfFailed(m_commandList->Close());

	// Befehlsliste ausführen.
	ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
	m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	return true;
}
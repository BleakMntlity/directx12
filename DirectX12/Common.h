#pragma once

#include <DirectXMath.h>

namespace DirectX12 
{
	void Cross(const DirectX::XMFLOAT3& first, const DirectX::XMFLOAT3& second, DirectX::XMFLOAT3* out)
	{
		out->x = first.y * second.z - first.z * second.y;
		out->y = first.z * second.x - first.x * second.z;
		out->z = first.x * second.y - first.y * second.x;
	}
}

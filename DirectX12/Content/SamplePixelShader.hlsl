Texture2D MainTex : register(t0);

cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix modelMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	float3 lightDirection;
	float3 cameraPosition;
};

SamplerState Sampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

// Pro-Pixel-Farbdaten an den Pixelshader �bergeben.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float2 texcoord : TEXCOORD;
	float diffuseLightFactor : TEXCOORD1;
	float3 worldPos : TEXCOORD2;
	float3 worldNormal : TEXCOORD3;
};

// Eine Pass-Through-Funktion f�r die (interpolierten) Farbdaten.
float4 main(PixelShaderInput input) : SV_TARGET
{
	float shinyness = 8;
	float3 glossyColor = float3(1, 1, 1);

	float3 lookAtDirection = normalize(input.worldPos - cameraPosition);

	float3 halfwayVector = normalize(lookAtDirection + normalize(lightDirection));
	float3 specularReflection = pow(max(dot(input.worldNormal, halfwayVector), 0.0), 8) * shinyness * glossyColor;
	float4 diffuseReflection = MainTex.Sample(Sampler, input.texcoord) * input.diffuseLightFactor;
	return float4(diffuseReflection.rgb + specularReflection, diffuseReflection.a);
}

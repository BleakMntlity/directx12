// Ein Konstantenpuffer, der die drei grundlegenden spaltengerichteten Matrizen f�r das Zusammensetzen von Geometrien speichert.
cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix modelMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	float3 lightDirection;
	float3 cameraPosition;
};

// Pro-Vertex-Daten als Eingabe f�r den Vertex-Shader verwendet.
struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 color : COLOR0;
	float2 texcoord : TEXCOORD;
	float3 normal: NORMAL;
};

// Pro-Pixel-Farbdaten an den Pixelshader �bergeben.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float2 texcoord : TEXCOORD;
	float diffuseLightFactor: TEXCOORD1;
	float3 worldPos : TEXCOORD2;
	float3 worldNormal : TEXCOORD3;
};

// Einfacher Shader f�r Vertex-Bearbeitung auf der GPU.
PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);
	output.worldNormal = input.normal;

	// Die Position des Scheitelpunkts in den projektierten Raum transformieren.
	pos = mul(pos, modelMatrix);
	output.worldPos = pos.xyz;

	pos = mul(pos, viewMatrix);
	pos = mul(pos, projectionMatrix);
	output.pos = pos;
	
	output.diffuseLightFactor = dot(output.worldNormal, normalize(lightDirection));

	// Die Farbe unver�ndert durchlaufen.
	output.color = input.color;
	output.texcoord = input.texcoord;

	return output;
}

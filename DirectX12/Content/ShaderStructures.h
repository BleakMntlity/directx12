﻿#pragma once

namespace DirectX12
{
	// Konstantenpuffer zum Senden von MVP-Matrizen an den Vertex-Shader verwendet.
	struct ConstantBuffer
	{
		DirectX::XMFLOAT4X4 modelMatrix;
		DirectX::XMFLOAT4X4 viewMatrix;
		DirectX::XMFLOAT4X4 projectionMatrix;
		DirectX::XMFLOAT3 lightDirection;
		DirectX::XMFLOAT3 cameraPosition;
	};

	// Verwendet zum Senden von Pro-Vertex-Daten an den Vertex-Shader.
	struct VertexPositionColor
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 color;
	};

	struct BasicVertexStruct {
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 color;
		DirectX::XMFLOAT2 texcoord;
		DirectX::XMFLOAT3 normal;
	};
}